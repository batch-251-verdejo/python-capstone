from abc import ABC, abstractclassmethod

class Person(ABC):
    @abstractclassmethod

    # Abstraction
    def getFullName(self):
        pass

    def addRequest():
        pass

    def checkRequest():
        pass
    
    def createUser(self, fname, lname, email, dept):
        self._firstName = fname
        self._lastName = lname
        self._email = email
        self._department = dept
    
    
class Employee(Person):
    def __init__(self):        
        self._firstName = ""
        self._lastName = ""
        self._email = ""
        self._department = ""
    
    # Getter
    def getDetails(self):
        print(f"{self._firstName} {self._lastName}, {self._email}, {self._department}")

    def getFirstName(self):
        print(f"{self._firstName}")
    
    def getLastName(self):
        print(f"{self._lastName}")
    
    def getFullName(self):
        print(self._firstName + " " + self._lastName)

    def getEmail(self):
        print(f"{self._email}")
    
    def getDepartment(self):
        print(f"{self._department}")
    
    # Setter    
    def set_firstName(self, fname):
        self._firstName = fname
    
    def set_lastName(self, lname):
        self._lastName = lname
    
    def set_email(self, email):
        self._email = email
    
    def set_department(self, department):
        self._department = department
        
    # Abstraction
    def createUser(self, fname, lname, email, dept):
        self._firstName = fname
        self._lastName = lname
        self._email = email
        self._department = dept
    
    def addUser(self):
        print(f"User has been added")

    def addRequest(self):
        print("Request has been added.")

    def checkRequest():
        pass

    def login(self):
        print(f"{self._email} has logged in.")

    def logout(self):
        print(f"{self._email} has logged out.")

class TeamLead(Person):
    def __init__(self):        
        self._firstName = ""
        self._lastName = ""
        self._email = ""
        self._department = ""

        # Other properties
        self.indiv_emp = []
    
    # Getter
    def getDetails(self):
        print(f"{self._firstName} {self._lastName}, {self._email}, {self._department}")

    def getFirstName(self):
        print(f"{self._firstName}")
    
    def getLastName(self):
        print(f"{self._lastName}")
    
    def getFullName(self):
        print(self._firstName + " " + self._lastName)

    def getEmail(self):
        print(f"{self._email}")
    
    def getDepartment(self):
        print(f"{self._department}")
    
    def get_members(self):
        return self.indiv_emp
    
    # Setter    
    def set_firstName(self, fname):
        self._firstName = fname
    
    def set_lastName(self, lname):
        self._lastName = lname
    
    def set_email(self, email):
        self._email = email
    
    def set_department(self, department):
        self._department = department
    
    # Abstraction
    def createUser(self, fname, lname, email, dept):
        self._firstName = fname
        self._lastName = lname
        self._email = email
        self._department = dept
    
    def addUser(self):
        print(f"User has been added")

    def addMember(self, name):
        self.indiv_emp.append(name)
        return self.indiv_emp
    
    def addRequest(self):
        print("Request has been added.")

    def checkRequest():
        pass
        
    def login(self):
        print(f"{self._email} has logged in.")

    def logout(self):
        print(f"{self._email} has logged out.")

class Admin(Person):
    def __init__(self):        
        self._firstName = ""
        self._lastName = ""
        self._email = ""
        self._department = ""
    
    # Getter
    def getDetails(self):
        print(f"{self._firstName} {self._lastName}, {self._email}, {self._department}")

    def getFirstName(self):
        print(f"{self._firstName}")
    
    def getLastName(self):
        print(f"{self._lastName}")
    
    def getFullName(self):
        print(self._firstName + " " + self._lastName)

    def getEmail(self):
        print(f"{self._email}")
    
    def getDepartment(self):
        print(f"{self._department}")
    
    # Setter    
    def set_firstName(self, fname):
        self._firstName = fname
    
    def set_lastName(self, lname):
        self._lastName = lname
    
    def set_email(self, email):
        self._email = email
    
    def set_department(self, department):
        self._department = department
    
    # Abstraction
    def createUser(self, fname, lname, email, dept):
        self._firstName = fname
        self._lastName = lname
        self._email = email
        self._department = dept
    
    def addUser(self):
        print(f"User has been added")
    
    def addMember():
        pass        
    
    def addRequest():
        print("Request has been added.")

    def checkRequest():
        pass

    def login(self):
        print(f"{self._email} has logged in.")

    def logout(self):
        print(f"{self._email} has logged out.")

class Request():
    def __init__(self, name, requester, dateRequested, status):
        self._name = name
        self._requester = requester
        self._dateRequested = dateRequested
        self._status = status

    
    def getDetails(self):
        print(f"{self._name} {self._requester}, {self._dateRequested}, {self._status}")

    # methods
    def set_status(self, status):
        self._status = status

    def updateRequest(self):
        self._status = "on-going"

    def closeRequest(self):
        self._status = "closed"
        print(f"{self._name} has been {self._status}.")
    
    def cancelRequest(self):
        self._status = "cancelled"
        print(f"{self._name} has been {self._status}.")



teamleader1 = TeamLead()
employee1 = Employee()
employee2 = Employee()
employee3 = Employee()
employee4 = Employee()
admin1 = Admin()
req1 = Request("Desktop crash", employee1, "01/01/2023", "open")
req2 = Request("UPS exploded", employee2, "01/01/2023", "open")

employee1.createUser("Random", "Surname", "user2@gmail.com", "Frontend")
employee2.createUser("Haffaz", "Aladeen", "dictator@gmail.com", "Frontend")
employee3.createUser("Nuclear", "Nadal", "nadal@gmail.com", "Frontend")
employee4.createUser("Sayed", "Badreya", "father@gmail.com", "Frontend")
teamleader1.createUser("Rowell", "Verdejo", "user1@gmail.com", "Frontend")
admin1.createUser("Anonymous", "Surname2", "user3@gmail.com", "Frontend")

assert employee1.getFullName == "Random Surname", "Full name should be Random Surname"
assert admin1.getFullName == "Anonymous Surname2", "Full name should be Anonymous Surname2"
assert teamleader1.getFullName == "Rowell Verdejo", "Full name should be Rowell Verdejo"
assert employee2.login == "dictator@gmail.com has logged in."
assert employee2.addRequest == "Request has been added."
assert employee2.logout == "dictator@gmail.com has logged out."


teamleader1.addMember(employee1)
teamleader1.addMember(employee2)

for indiv_emp in teamleader1.get_members():
    print(indiv_emp.getFullName())

req2.set_status("closed")
print(req2.closeRequest())
